#version 140

out vec4 color;
uniform vec2 u_resolution;
uniform float u_time;
uniform sampler2D u_prevframe;

void main() {
    vec2 uv = gl_FragCoord.xy/u_resolution;

    vec2 gridsize = 128. * vec2(1.);
    vec2 griduv = floor(uv * gridsize) / gridsize;

    float count = 0.0;
    for (float i = -1.; i<= 1.0; i+=1.)
      for (float j = -1.; j<= 1.0; j+=1.)
	{
	  if (i == 0 && j == 0)
	    continue;
	  vec4 nei = texture2D(u_prevframe, griduv + ((vec2(i, j) + .5) / gridsize)); 
	  count += floor(nei.x);
	}

    float state = texture2D(u_prevframe, griduv + .5 / gridsize).g;
    float d = count == 3.0 ? 1.0 : max(state - .5, 0.0);
    if(state > .9){
      d = count == 2.0 ? 1.0 : d;
    }
    if(u_time < .7)
      d = 0.6 < fract(50321.0*sin((80.0* length(griduv - .5) + u_time))) ? 1.0 : 0.0;
    vec3 col = vec3(state, d, 0.0);
    color = vec4(vec3(d), 1.0);
    //color = vec4(griduv, 0.0, 1.0);
}
