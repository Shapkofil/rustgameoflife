#version 140

out vec4 color;
uniform vec2 u_resolution;
uniform vec2 u_mousepos;
uniform bool u_mousehold;
uniform float u_time;
uniform sampler2D u_prevframe;

#define PI 3.14159265359

vec2 evalBezier(in float t, vec2 a, vec2 b, vec2 c, vec2 d)
{
  //Using bernstein equations to evaluate a cubic Bezier
  vec4 ws =
    mat4(-1.,  3., -3., 1.,
	  3., -6.,  3., 0., 
	 -3.,  3.,  0., 0.,
	  1.,  0.,  0., 0.) *
    vec4(pow(t,3.), pow(t,2.), t, 1.);
  return vec2(
	      a * ws.x +
	      b * ws.y +
	      c * ws.z +
	      d * ws.w);
}

vec3 activation(vec3 x){
  return vec3(
	      evalBezier(clamp(x.z, 0.0, 1.0),
			 vec2(0,0),
			 vec2(0.25,1.0 + x.y * 10.0),
			 vec2(0.75,0.0 - x.y * 10.0),
			 vec2(1,1.0-clamp(x.z, 0.0, 1.0))
			 ).y,
	      evalBezier(clamp(x.x, 0.0, 1.0),
			 vec2(0,0),
			 vec2(0.25,1.0 + x.z * 10.0),
			 vec2(0.75,0.0 - x.z * 10.0),
			 vec2(1,1.0-clamp(x.z, 0.0, 1.0))
			 ).y,
	      evalBezier(clamp(x.y, 0.0, 1.0),
			 vec2(0,0),
			 vec2(0.25,1.0 + x.x * 10.0),
			 vec2(0.75,0.0 - x.x * 10.0),
			 vec2(1,1.0-clamp(x.z, 0.0, 1.0))
			 ).y
	      );
}

mat3 R(float t, vec2 uv){
    mat3 transform = mat3(cos(t), 0.0,-sin(t),
			  0.0, 1.0, 0.0,
			  sin(t), 0.0, cos(t)) * (fract(-t * .2) + max(.2 - length(uv - .5), 0.0));
    return transform;
}

void main() {
    vec2 uv = gl_FragCoord.xy/u_resolution.y;
    vec2 mouseuv = u_mousepos/u_resolution.y;
    mouseuv.y = 1.0 - mouseuv.y; 

    vec2 gridsize =  vec2(1.) / 256.0;
    vec2 gridoffset = vec2(u_resolution.y/u_resolution.x, 1.0) * gridsize;
    vec2 griduv = floor(uv / gridsize) * gridsize;

    mat3 kernel = mat3(-.15, -0.5, .25,
		       0.5, .7, -0.1,
		       0.15, 0.1, -.25) * .25 ;


    
    mat3 transform = R(u_time * .2, griduv);

    vec3 sum = vec3(0.0);
    for (float i = -1.; i<= 1.0; i+=1.)
      for (float j = -1.; j<= 1.0; j+=1.)
	{
	  vec4 nei = texture2D(u_prevframe,
			       griduv/gridsize*gridoffset +
			       ((vec2(i, j) + .5) * gridoffset)); 
	  mat3 M = transform * kernel;
	  sum += nei.rgb * M[int(i + 1.0)][int(j + 1.0)];
	}

    vec3 d = u_time > .1 ? activation(sum) : vec3(0.0);
    if(u_mousehold){
      float stencil = smoothstep(.01, 0.0, length(mouseuv - griduv) - .05);
      d += stencil;
      d = clamp(d, 0.0, 1.0);}
    color = vec4(d, 1.0);
    //color = vec4(griduv/gridsize*gridoffset, 0.0, 1.0);
}
