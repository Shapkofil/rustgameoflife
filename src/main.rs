#[macro_use]
extern crate glium;

use std::error::Error;

use glium::{
    glutin::{
	dpi::{PhysicalSize, PhysicalPosition},
	event_loop::{ControlFlow}},
    Program, Display};

#[derive(Copy, Clone)]
struct Vertex {
    position: [f32; 2],
}

fn unpack_physical_size(size: &PhysicalSize<u32>) -> [f32; 2]{
    let size = size.clone();
    [size.width as f32, size.height as f32]
}

fn unpack_physical_position(pos: &PhysicalPosition<f64>) -> [f32; 2]{
    let pos = pos.clone();
    [pos.x as f32, pos.y as f32]
}

fn compile_gpu_program(display: &Display) -> Result<Program, Box<dyn Error>> {
   let vertex_shader_src = r#"
        #version 140
        in vec2 position;

        void main() {
            gl_Position = vec4(position, 0.0, 1.0);
        }
    "#;

    let fragment_shader_src_owned = std::fs::read_to_string("shader.frag")?;
    let fragment_shader_src= fragment_shader_src_owned.as_str();
    let program = glium::Program::from_source(display, vertex_shader_src, fragment_shader_src, None)?;
    Ok(program)
}

fn main(){
    #[allow(unused_imports)]
    use glium::{glutin, Surface};

    let event_loop = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new();
    let cb = glutin::ContextBuilder::new();
    let display = glium::Display::new(wb, cb, &event_loop).unwrap();

    implement_vertex!(Vertex, position);

    let vertex1 = Vertex { position: [-1.0, -1.0] };
    let vertex2 = Vertex { position: [-1.0, 1.0] };
    let vertex3 = Vertex { position: [1.0, 1.0] };
    let vertex4 = Vertex { position: [1.0, -1.0] };
    let shape = vec![vertex1, vertex2, vertex3, vertex1, vertex3, vertex4];

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);

    const TARGET_FPS: u64 = 10;

    let mut program =
	compile_gpu_program(&display).unwrap();

    let mut spawn_time = std::time::Instant::now();

    let mut mouseheld: bool = false;
    let mut mousepos  = [0.0, 0.0];
    event_loop.run(move |event, _, control_flow| {
	*control_flow = ControlFlow::Poll;
	let start_time = std::time::Instant::now();
        match event {
            glutin::event::Event::WindowEvent { event , ..}  => match event {
                glutin::event::WindowEvent::CloseRequested => {
                    *control_flow = glutin::event_loop::ControlFlow::Exit;
                    return;
                },
		glutin::event::WindowEvent::CursorMoved { position, ..} => {
		    mousepos = unpack_physical_position(&position);
		},
		glutin::event::WindowEvent::MouseInput {
		    state,
		    button:glutin::event::MouseButton::Left, ..} => {
		    mouseheld = match state {
			glutin::event::ElementState::Pressed => true,
			glutin::event::ElementState::Released => false,
		    };
		},
		glutin::event::WindowEvent::KeyboardInput {
		    input: glutin::event::KeyboardInput {
			virtual_keycode:Some(code),
			.. },
		    .. } => {
		    match code {
			glutin::event::VirtualKeyCode::C => {
			    program = compile_gpu_program(&display).unwrap();
			    spawn_time = std::time::Instant::now();
			},
			key => println!("{:?} pressed", key)
		    }
		}
                _ => return,
            },
	    glutin::event::Event::MainEventsCleared => {
		let elapsed_time = std::time::Instant::now()
		    .duration_since(start_time).as_millis() as u64;
		let wait_millis = match 1000 / TARGET_FPS >= elapsed_time {
		    true => 1000 / TARGET_FPS - elapsed_time,
		    false => 0
		};
		let new_inst = start_time + std::time::Duration::from_millis(wait_millis);
		*control_flow =  glutin::event_loop::ControlFlow::WaitUntil(new_inst);
		let elapsed_time = std::time::Instant::now()
		    .duration_since(spawn_time).as_millis() as u64;
		
		let prevtexture: Vec<Vec<(u8, u8, u8, u8)>> = display.read_front_buffer().unwrap();
		let prevtexture =
		    glium::texture::Texture2d::new(&display, prevtexture).unwrap();
		let mut target = display.draw();
		target.clear_color(1.0, 0.0, 1.0, 1.0);
		
		let uniforms = uniform! {
		    u_time: elapsed_time as f32 / 1000.0,
		    u_resolution: unpack_physical_size(
			&(display.gl_window().window().inner_size())),
		    u_prevframe: prevtexture.sampled()
			.minify_filter(glium::uniforms::MinifySamplerFilter::Nearest),
		    u_mousepos: mousepos,
		    u_mousehold: mouseheld
		};
		
		target.draw(&vertex_buffer, &indices, &program, &uniforms,
			    &Default::default()).unwrap();
		target.finish().unwrap();
		
	    }
            _ => return,
        }
    });
}
