#version 140

out vec4 color;
uniform vec2 u_resolution;
uniform vec2 u_mousepos;
uniform bool u_mousehold;
uniform float u_time;
uniform sampler2D u_prevframe;

#define PI 3.14159265359

vec2 evalBezier(in float t)
{
  //Using bernstein equations to evaluate a cubic Bezier
  vec4 ws =
    mat4(-1.,  3., -3., 1.,
	  3., -6.,  3., 0., 
	 -3.,  3.,  0., 0.,
	  1.,  0.,  0., 0.) *
    vec4(pow(t,3.), pow(t,2.), t, 1.);
  return vec2(
	      vec2(0,0) * ws.x +
	      vec2(0.25,2.0) * ws.y +
	      vec2(0.75,-1) * ws.z +
	      vec2(1,0) * ws.w);
}

float activation(float x){
  return evalBezier(clamp(x, 0.0, 1.0)).y;
}

void main() {
    vec2 uv = gl_FragCoord.xy/u_resolution;
    vec2 mouseuv = u_mousepos/u_resolution;
    mouseuv.y = 1.0 - mouseuv.y; 

    vec2 gridsize = 256. * vec2(1.);
    vec2 griduv = floor(uv * gridsize) / gridsize;

    mat3 kernel = mat3(-.15, 0.5, .25,
		       -0.5, .7, -0.1,
		       0.15, 0.1, -.25) * .25 ;

    float sum = 0.0;
    for (float i = -1.; i<= 1.0; i+=1.)
      for (float j = -1.; j<= 1.0; j+=1.)
	{
	  vec4 nei = texture2D(u_prevframe, griduv + ((vec2(i, j) + .5) / gridsize)); 
	  sum += nei.x * kernel[int(i + 1.0)][int(j + 1.0)];
	}

    float d = u_time > .1 ? activation(sum) : 0.0;
    if(u_mousehold)
      d += smoothstep(.01, 0.0, length(mouseuv - griduv) - .02);
      d = clamp(d, 0.0, 1.0);
    color = vec4(vec3(d), 1.0);
    //color = vec4(griduv, 0.0, 1.0);
}
